#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>   //get_timeofday(), struct timeval
#include <time.h>       //gmtime(), localtime(), struct tm 
#include "DRV8830.hpp"

int main(int argc, char* argv[]){
	int i, ret;
	char str[256];
	char *com, *period, *arg1, *arg2;
	struct timeval tm, tm0;
	double speed;
	FILE *fp;

	DRV8830 MotR, MotL;

//Read Command file
	if(argc != 2){
		printf("Usage: %s [command file]\n",argv[0]);
		return -1;
	}
	if(NULL == (fp=fopen(argv[1], "r"))){
		printf("File Open Error: %s\n", argv[1]);
		return -1;
	}


//Init Motor Drivers
	int fdR = MotR.Init( 0x63 );
	int fdL = MotL.Init( 0x64 );

	printf("fd(R)=%d\n", fdR);
	printf("fd(L)=%d\n", fdL);

	if( (fdR<0) || (fdL<0)){
		return -1;
	}

	MotR.PrintFaultMsg( MotR.ReadFault() );
	MotL.PrintFaultMsg( MotL.ReadFault() );


	while( ! feof(fp) ){

//Read commnad from file
		fgets(str, 256, fp);
		printf("%s", str);
		com = strtok(str, " ");

//Comment, blank line -> skip
		if( com == NULL) continue;
		if( strlen(com) == 0) continue;
		if( (com[0] == '#') || com[0] == '\n') continue;

		period = strtok(NULL, " ");
		arg1 = strtok(NULL, " ");
		arg2 = strtok(NULL, " ");


		if( !strncmp( com, "FWD", 3 ) ){	//Forward
			speed = atof(arg1) * 5.06;	// 0 < arg1 < 1
			MotR.Run( -speed );
			MotL.Run( speed );

		}else if( !strncmp( com, "REV", 3) ){	//Reverse
			speed = atof(arg1) * 5.06;	// 0 < arg1 < 1
			MotR.Run( speed );
			MotL.Run( -speed );

		}else if( !strncmp( com, "ROT", 3) ){	//Rotate
			if( arg1[0] == 'R' ){
				speed = atof(arg2) * 5.06 ;	// 0 < arg1 < 1
				MotR.Run( speed );
				MotL.Run( speed );

			}else if( arg1[0] == 'L'){
				speed = atof(arg2) * 5.06 ;	// 0 < arg1 < 1
				MotR.Run( -speed );
				MotL.Run( -speed );
			}

		}else if( !strncmp(com, "STOP", 4) ){
			MotR.Stop();
			MotL.Stop();
			
		}else if( !strncmp(com, "BRK", 3) ){
			MotR.Brake();
			MotL.Brake();

		}else if( !strncmp(com, "END", 3) ){
			MotR.Stop();
			MotL.Stop();
			break;
			
		}else{
			MotR.Stop();
			MotL.Stop();
		}

		usleep( atof(period)*1e6 );

	}//end of while()

	MotR.Stop();
	MotL.Stop();

	fclose(fp);

}
